//
//  Macros.h
//  RemoteBot for iOS
//
//  Created by Michal Bínovský on 4/19/13.
//  Copyright (c) 2013 Michal Bínovský. All rights reserved.
//
#pragma once

#ifndef max
#define max(a,b) ( ( (a)>(b) ) ? (a) : (b) )
#endif

extern NSString *const NOTIFY_INTERFACEWILLROTATE;

// FONT
extern CGFloat const DEFAULT_FONT_SIZE;
extern NSString *const DEFAULT_FONT_NAME;

#define DEFAULT_FONT(fontSize)      ( fontSize ) ? [UIFont systemFontOfSize:fontSize] : [UIFont systemFontOfSize:DEFAULT_FONT_SIZE]
#define DEFAULT_BOLD_FONT(fontSize) ( fontSize ) ? [UIFont boldSystemFontOfSize:fontSize] : [UIFont boldSystemFontOfSize:DEFAULT_FONT_SIZE]
#define SEARCHFIELD_FONT [UIFont systemFontOfSize:13]

// BUNDLE
#define MAIN_BUNDLE()       [NSBundle mainBundle]