//
//  Macros.m
//  RemoteBot for iOS
//
//  Created by Michal Bínovský on 4/19/13.
//  Copyright (c) 2013 Michal Bínovský. All rights reserved.
//

// NOTIFICATIONS
NSString *const NOTIFY_INTERFACEWILLROTATE = @"NotifyInterfaceWillRotate";

// FONT
CGFloat const DEFAULT_FONT_SIZE = 18.f;
NSString *const DEFAULT_FONT_NAME = @"AvenirNextCondensed-Regular";