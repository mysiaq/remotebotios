//
//  UIFont+GlobalCustom.h
//  RemoteBot for iOS
//
//  Created by Michal Bínovský on 4/19/13.
//  Copyright (c) 2013 Michal Bínovský. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (GlobalCustomFont)

- (UIFont *)font;
+ (UIFont *)font;

@end
