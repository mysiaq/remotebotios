//
//  UIFont+GlobalCustom.m
//  RemoteBot for iOS
//
//  Created by Michal Bínovský on 4/19/13.
//  Copyright (c) 2013 Michal Bínovský. All rights reserved.
//

#import "UILabel+GlobalCustomFont.h"

@implementation UILabel (GlobalCustomFont)

-(UIFont *)font
{
    return [UIFont fontWithName:DEFAULT_FONT_NAME size:DEFAULT_FONT_SIZE];
}

+(UIFont *)font
{
    return [UIFont fontWithName:DEFAULT_FONT_NAME size:DEFAULT_FONT_SIZE];
}


@end
