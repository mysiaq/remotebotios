//
//  UIFont+CustomSystem.m
//  RemoteBotiOS
//
//  Created by Michal Bínovský on 4/22/13.
//  Copyright (c) 2013 Michal Bínovský. All rights reserved.
//

#import "UIFont+CustomSystem.h"

@implementation UIFont (CustomSystem)

+ (UIFont *)sysFontOfSize:(CGFloat)fontSize
{
	return [UIFont fontWithName:DEFAULT_FONT_NAME size:fontSize];
}

@end
