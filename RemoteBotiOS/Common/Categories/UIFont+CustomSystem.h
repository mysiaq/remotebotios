//
//  UIFont+CustomSystem.h
//  RemoteBotiOS
//
//  Created by Michal Bínovský on 4/22/13.
//  Copyright (c) 2013 Michal Bínovský. All rights reserved.
//
@interface UIFont (CustomSystem)

+ (UIFont *)sysFontOfSize:(CGFloat)fontSize;

@end
