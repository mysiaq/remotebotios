//
//  main.m
//  RemoteBot for iOS
//
//  Created by Michal Bínovský on 4/14/13.
//  Copyright (c) 2013 Michal Bínovský. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MBAppDelegate.h"

int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([MBAppDelegate class]));
	}
}
