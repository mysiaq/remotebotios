//
//  MBAppDelegate.h
//  RemoteBot for iOS
//
//  Created by Michal Bínovský on 4/14/13.
//  Copyright (c) 2013 Michal Bínovský. All rights reserved.
//
#define APP_DELEGATE    ((MBAppDelegate *)[[UIApplication sharedApplication] delegate])
#define	ROOT_CONTROLLER ((MBViewController *)[((MBAppDelegate *)[[UIApplication sharedApplication] delegate]) viewController])

@class MBViewController;

@interface MBAppDelegate : UIResponder < UIApplicationDelegate >

@property ( strong, nonatomic )	UIWindow			*window;
@property ( strong, nonatomic )	MBViewController	*viewController;

@end
