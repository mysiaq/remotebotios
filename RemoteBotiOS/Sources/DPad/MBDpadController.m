//
//  MBDpadControllerViewController.m
//  RemoteBot for iOS
//
//  Created by Michal Bínovský on 4/19/13.
//  Copyright (c) 2013 Michal Bínovský. All rights reserved.
//

#import "MBDpadController.h"

static CGFloat const CONTROLLER_POSITION_DOT_SIZE = 10.f;

typedef enum
{
	DpadQuadrantNotAnQuadrant	= 0,
	DpadQuadrant1				= 1,
	DpadQuadrant2				= 2,
	DpadQuadrant3				= 3,
	DpadQuadrant4				= 4
	
}DpadTouchQuadrant;

#pragma mark
#pragma mark @PrivateInterface
@interface MBDpadController ()

	@property ( nonatomic, copy )	NSString	*strDpadController;
	@property ( nonatomic, copy )	NSString	*strJoystick;
	@property ( nonatomic, retain )	UIImage		*imgDpadController;
	@property ( nonatomic, retain )	UIImage		*imgJoystick;
	@property ( nonatomic, assign ) CGFloat		dpadOriginX;
	@property ( nonatomic, assign ) CGFloat		dpadOriginY;
	@property ( nonatomic, assign ) CGFloat		joystickOriginX;
	@property ( nonatomic, assign ) CGFloat		joystickOriginY;

	- (void)resetCenterPosition;
	- (DpadTouchQuadrant)coordsQuadrantForX:(CGFloat) aX andY:(CGFloat)aY;
	- (CGPoint)transformToUnitCircle:(CGPoint)point;

@end

#pragma mark
#pragma mark MBDpadController
@implementation MBDpadController

@synthesize delegate				= _delegate;
@synthesize strDpadController		= _strDpadController;
@synthesize strJoystick				= _strJoystick;
@synthesize imgDpadController		= _imgDpadController;
@synthesize imgJoystick				= _imgJoystick;
@synthesize dpadOriginX				= _dpadOriginX;
@synthesize dpadOriginY				= _dpadOriginY;
@synthesize joystickOriginX			= _joystickOriginX;
@synthesize joystickOriginY			= _joystickOriginY;

#pragma mark
#pragma mark @Initialization
- (id)initWithFrame:(CGRect)frame
{
    if ( (self = [super initWithFrame:frame]) )
	{
        // Initialization code
    }
    return self;
}

- (id)initWithControllerImgName:(NSString *)strController andWithJoysticImgName:(NSString *)strJoystick
{
	if ( (self = [super init]) )
	{
		// Initialization code
		[self setTranslatesAutoresizingMaskIntoConstraints:NO];
		if ( strController != nil && strController.length > 0 )
			self.strDpadController = strController;
		else
			[NSException raise:@"Invalid argument" format:@"Invalid value of input parameter 'strController' was given.", nil];
		
		if ( strJoystick != nil && strJoystick.length > 0 )
			self.strJoystick = strJoystick;
		else
			[NSException raise:@"Invalid argument" format:@"Invalid value of input parameter 'strJoystick' was given.", nil];
		
		_imgDpadController = [UIImage imageNamed:_strDpadController];
		[_imgDpadController retain];
		
		_imgJoystick = [UIImage imageNamed:_strJoystick];
		[_imgJoystick retain];
		
		[self resetCenterPosition];
	}
	
	return self;
}

- (void)dealloc
{
	[_strDpadController release]; _strDpadController = nil;
	[_strJoystick release]; _strJoystick = nil;
	[_imgDpadController release]; _imgDpadController = nil;
	[_imgJoystick release]; _imgJoystick = nil;
	[super dealloc];
}

#pragma mark
#pragma mark @Override
 - (void)drawRect:(CGRect)rect
{
	 if ( !( _dpadOriginX >= 0.f ) || !( _dpadOriginY >= 0.f ) || _imgDpadController == nil || _imgJoystick == nil )
		 return;
	
	[_imgDpadController drawAtPoint:CGPointMake( _dpadOriginX, _dpadOriginY )];
	[_imgJoystick drawAtPoint:CGPointMake( _joystickOriginX, _joystickOriginY )];
	
	CGContextRef context = UIGraphicsGetCurrentContext();
	
	CGFloat joystickRadius = ( _imgJoystick.size.width / 2 );
	CGFloat dpadRadius = ( _imgDpadController.size.width / 2 );
	
	CGFloat joystickMiddleX = _joystickOriginX + joystickRadius;
	CGFloat joystickMiddleY = _joystickOriginY + joystickRadius;
	
	CGFloat dpadMiddleX = _dpadOriginX + dpadRadius;
	CGFloat dpadMiddleY = _dpadOriginY + dpadRadius;
	
	CGFloat joysticDpadDistance = sqrt( pow(joystickMiddleX - dpadMiddleX, 2) +  pow(joystickMiddleY - dpadMiddleY, 2) );
	
	CGFloat redDotOriginX = -1.f;
	CGFloat redDotOriginY = -1.f;
	
	if ( joysticDpadDistance <= dpadRadius )
	{
		redDotOriginX = _joystickOriginX + ( _imgJoystick.size.width / 2 ) - ( CONTROLLER_POSITION_DOT_SIZE / 2 );
		redDotOriginY = _joystickOriginY + ( _imgJoystick.size.height / 2 ) - ( CONTROLLER_POSITION_DOT_SIZE / 2 );
	}
	else
	{
		DpadTouchQuadrant touchQuadrant = [self coordsQuadrantForX:joystickMiddleX andY:joystickMiddleY];
		
		CGFloat sX = joystickMiddleX - dpadMiddleX;
		CGFloat sY = joystickMiddleY - dpadMiddleY;
		
		CGFloat a = pow( sX, 2 ) + pow( sY, 2 );
		CGFloat b = 0;
		CGFloat c = (-1)* pow( dpadRadius, 2 );
		
		// Discriminant
		CGFloat d = pow( b, 2 ) - ( 4 * a * c );
		CGFloat D = sqrt( d );
		
		CGFloat D1 = ( -b + D ) / ( 2 * a );
		CGFloat D2 = ( -b - D ) / ( 2 * a );
		
		
		// X 1,2
		CGFloat X1 = dpadMiddleX + sX*D1;;
		CGFloat X2 = dpadMiddleX + sX*D2;
		
		// Y 1,2
		CGFloat Y1 = dpadMiddleY + sY*D1;
		CGFloat Y2 = dpadMiddleY + sY*D2;
		
		DpadTouchQuadrant quadrant1 = [self coordsQuadrantForX:X1 andY:Y1];
		DpadTouchQuadrant quadrant2 = [self coordsQuadrantForX:X2 andY:Y2];
		
		if ( quadrant1 == touchQuadrant )
		{
			redDotOriginX = X1;
			redDotOriginY = Y1;
		}
		else if ( quadrant2 == touchQuadrant )
		{
			redDotOriginX = X2;
			redDotOriginY = Y2;
		}
		else
		{
			NSLog( @"FATAL ERROR IN CALCULATIONS!" );
			redDotOriginX = dpadMiddleX;
			redDotOriginY = dpadMiddleY;
		}
		
		// drawing rect origin correction
		redDotOriginX -= CONTROLLER_POSITION_DOT_SIZE / 2;
		redDotOriginY -= CONTROLLER_POSITION_DOT_SIZE / 2;
	}
	
	CGContextSetFillColorWithColor( context, [UIColor redColor].CGColor );
	CGContextSetAlpha( context, 0.5f );
	CGContextFillEllipseInRect( context, CGRectMake( redDotOriginX, redDotOriginY, CONTROLLER_POSITION_DOT_SIZE, CONTROLLER_POSITION_DOT_SIZE ) );
	
	CGContextSetStrokeColorWithColor( context, [UIColor blackColor].CGColor );
	CGContextStrokeEllipseInRect( context, CGRectMake( redDotOriginX, redDotOriginY, CONTROLLER_POSITION_DOT_SIZE, CONTROLLER_POSITION_DOT_SIZE ) );
	
	CGPoint unitCirclePoint = [self transformToUnitCircle:CGPointMake( redDotOriginX + (CONTROLLER_POSITION_DOT_SIZE / 2), redDotOriginY + (CONTROLLER_POSITION_DOT_SIZE / 2) )];
	
	if ( [_delegate respondsToSelector:@selector(dpadController:touchedAtPoint:)] )
		[_delegate dpadController:self touchedAtPoint:unitCirclePoint];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
	[super touchesBegan:touches withEvent:event];
	
	CGPoint point = [[touches anyObject] locationInView:self];
	
	CGPoint dpadOrigin = [self dpadControllerOriginForTouchCoords:point];
	self.dpadOriginX = dpadOrigin.x;
	self.dpadOriginY = dpadOrigin.y;
	
	CGPoint joystickOrigin = [self joystickOriginForTouchCoords:point];
	self.joystickOriginX = joystickOrigin.x;
	self.joystickOriginY = joystickOrigin.y;
	
	[self setNeedsDisplay];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
	[super touchesMoved:touches withEvent:event];
	
	CGPoint point = [[touches anyObject] locationInView:self];
	
	CGPoint joystickOrigin = [self joystickOriginForTouchCoords:point];
	self.joystickOriginX = joystickOrigin.x;
	self.joystickOriginY = joystickOrigin.y;
	
	[self setNeedsDisplay];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	[super touchesEnded:touches withEvent:event];
	[self resetCenterPosition];
	[self setNeedsDisplay];
	
	if ( [_delegate respondsToSelector:@selector(dpadController:touchedAtPoint:)] )
		[_delegate dpadController:self touchedAtPoint:CGPointZero];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
	[super touchesCancelled:touches withEvent:event];
	[self resetCenterPosition];
	[self setNeedsDisplay];
	
	if ( [_delegate respondsToSelector:@selector(dpadController:touchedAtPoint:)] )
		[_delegate dpadController:self touchedAtPoint:CGPointZero];
}

#pragma mark
#pragma mark @Custom
- (DpadTouchQuadrant)coordsQuadrantForX:(CGFloat) aX andY:(CGFloat)aY
{
	CGFloat dpadRadius = ( _imgDpadController.size.width / 2 );
	
	CGFloat dpadMiddleX = _dpadOriginX + dpadRadius;
	CGFloat dpadMiddleY = _dpadOriginY + dpadRadius;
	
	if ( aX > dpadMiddleX && aY < dpadMiddleY )
	{
		return DpadQuadrant1;
	}
	else if ( aX < dpadMiddleX && aY <= dpadMiddleY )
	{
		return DpadQuadrant2;
	}
	else if ( aX < dpadMiddleX && aY > dpadMiddleY )
	{
		return DpadQuadrant3;
	}
	else if ( aX > dpadMiddleX && aY > dpadMiddleY )
	{
		return DpadQuadrant4;
	}
	
	return DpadQuadrantNotAnQuadrant;
}

- (CGPoint)transformToUnitCircle:(CGPoint)point
{
	DpadTouchQuadrant touchQuadrant = [self coordsQuadrantForX:point.x andY:point.y];
	
	CGFloat dpadRadius = ( _imgDpadController.size.width / 2 );
	CGPoint dpadCeterPoint = CGPointMake( _dpadOriginX + dpadRadius, _dpadOriginY + dpadRadius );
	
	CGFloat distanceX = dpadCeterPoint.x - point.x;
	CGFloat distanceY = dpadCeterPoint.y - point.y;
	
	distanceX = ABS( distanceX );
	distanceY = ABS( distanceY );
	
	switch ( touchQuadrant )
	{
		case 1:
		{
			// Nothing to do here ... :P
		}
			break;
			
		case 2:
		{
			distanceX = -distanceX;
		}
			break;
			
		case 3:
		{
			distanceX = -distanceX;
			distanceY = -distanceY;
		}
			break;
			
		case 4:
		{
			distanceY = -distanceY;
		}
			break;
			
		default:
			break;
	}
	
	CGFloat onePercent = dpadRadius / 100;
	CGFloat unitCircleX = 0.f;
	CGFloat unitCircleY = 0.f;
	
	if ( distanceX == 0 )
		unitCircleX = 0.0;
	else
		unitCircleX = distanceX / onePercent;
	
	if ( distanceY == 0 )
		unitCircleY = 0.0;
	else
		unitCircleY = distanceY / onePercent;
	
	unitCircleX = unitCircleX / 100;
	unitCircleY = unitCircleY / 100;
	
	return CGPointMake( unitCircleX, unitCircleY );
}

- (void)resetCenterPosition
{
	self.dpadOriginX = -1.f;
	self.dpadOriginY = -1.f;
	self.joystickOriginX = -1.f;
	self.joystickOriginY = -1.f;
}

- (CGPoint)dpadControllerOriginForTouchCoords:(CGPoint)touchCoord
{
	if ( _imgDpadController == nil )
		return CGPointMake( -100.f, -100.f );
	
	CGSize dpadSize = [_imgDpadController size];
	CGFloat coordX = touchCoord.x - ( dpadSize.width / 2 );
	CGFloat coordY = touchCoord.y - ( dpadSize.height / 2 );
	
	return CGPointMake( coordX, coordY );
}

- (CGPoint)joystickOriginForTouchCoords:(CGPoint)touchCoord
{
	if ( _imgJoystick == nil )
		return CGPointMake( -1.f, -1.f );
	
	CGSize joystickSize = [_imgJoystick size];
	CGFloat coordX = touchCoord.x - ( joystickSize.width / 2 );
	CGFloat coordY = touchCoord.y - ( joystickSize.height / 2 );
	
	return CGPointMake( coordX, coordY );
}
@end