//
//  MBDpadControllerViewController.h
//  RemoteBot for iOS
//
//  Created by Michal Bínovský on 4/19/13.
//  Copyright (c) 2013 Michal Bínovský. All rights reserved.
//

@class MBDpadController;

@protocol MBDpadControllerDelegate <NSObject>

	@optional
	- (void)dpadController:(MBDpadController *)controller touchedAtPoint:(CGPoint)point;

@end

#pragma mark
#pragma mark MBDpadControllerPublicInterface
@interface MBDpadController : UIView
{
	#pragma mark
	#pragma mark @PublicIvars
	@public
		
	#pragma mark
	#pragma mark @ProtectedIvars
	@protected
	
	#pragma mark
	#pragma mark @PrivateIvars
	@private
}

	#pragma mark
	#pragma mark @Properties
	@property ( nonatomic, assign )	id<MBDpadControllerDelegate>	delegate;

	#pragma mark
	#pragma mark @MemberFunctions
	- (id)initWithControllerImgName:(NSString *)strController andWithJoysticImgName:(NSString *)strJoystick;

	#pragma mark
	#pragma mark @ClassFunctions

@end
