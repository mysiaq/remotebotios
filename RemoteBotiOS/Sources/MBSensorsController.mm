//
//  MBSensorsController.m
//  RemoteBot for iOS
//
//  Created by Michal Bínovský on 4/22/13.
//  Copyright (c) 2013 Michal Bínovský. All rights reserved.
//
#import "MBSensorsController.h"
#import "MBViewController.h"
#import "MBViewController.h"
#import "ApiManager/MBApiManager.h"

static NSString *const ROBOTINFO_BTN_TITLE				= @"Robot Info";
static NSString *const LIGHTSENSORBTN_BTN_TITLE			= @"Ligh Sensor Value";
static NSString *const TOUCHSENSORBTN_BTN_TITLE			= @"Touch Sensor State";

static NSString *const ROBOTINFO_WAITING_TITLE			= @"Fetching Robot Info";
static NSString *const LIGHTSENSORBTN_WAITING_TITLE		= @"Fetching Ligh Sensor Data";
static NSString *const TOUCHSENSORBTN_WAITING_TITLE		= @"Fetching Touch Sensor State";


#pragma mark
#pragma mark PrivateInterface
@interface MBSensorsController()

	@property ( nonatomic, retain )	MBApiManager			*apiManager;
	@property ( nonatomic, retain ) UILabel					*lbl;
	@property ( nonatomic, retain ) UIButton				*driveBtn;
	@property ( nonatomic, retain ) UIView					*sensorsContainer;
	@property ( nonatomic, retain ) UIButton				*robotInfoBtn;
	@property ( nonatomic, retain ) UIButton				*lightSensorBtn;
	@property ( nonatomic, retain ) UIButton				*touchSensorBtn;

	- (void)driveBtnTapped:(UIButton *)sender;
	- (void)createSensrosContainer;
	- (void)sensorBtnTapped:(UIButton *)sender;
	- (void)showAlertWithTitle:(NSString *)title andMessge:(NSString *)message;

@end

#pragma mark
#pragma mark MBSensorsController
@implementation MBSensorsController

@synthesize apiManager						= _apiManager;
@synthesize lbl								= _lbl;
@synthesize driveBtn						= _driveBtn;
@synthesize sensorsContainer				= _sensorsContainer;
@synthesize robotInfoBtn					= _robotInfoBtn;
@synthesize lightSensorBtn					= _lightSensorBtn;
@synthesize touchSensorBtn					= _touchSensorBtn;

- (id)init
{
	self = [super init];
	
	if ( self )
	{
		// Initialization code place here ...
	}
	
	return self;
}

- (void)dealloc
{
	[_apiManager release];
	[_lbl release];
	[_driveBtn release];
	[_sensorsContainer release];
	[_robotInfoBtn release];
	[_lightSensorBtn release];
	[_touchSensorBtn release];
	[super dealloc];
}

#pragma mark
#pragma mark Override
// Place here UIKit overriden functions
- (void)viewDidLoad
{
	[super viewDidLoad];
	
	if ( _lbl == nil )
	{
		_lbl = [[UILabel alloc] initWithFrame:CGRectZero];
		[_lbl setTranslatesAutoresizingMaskIntoConstraints:NO];
		[_lbl setBackgroundColor:[UIColor clearColor]];
		[_lbl setText:@"RemoteBotIOS Client App\n Sensors Screen"];
		[_lbl setNumberOfLines:2];
		[_lbl setAdjustsFontSizeToFitWidth:NO];
		[_lbl setTextColor:[UIColor whiteColor]];
		[_lbl setTextAlignment:NSTextAlignmentCenter];
		[_lbl setFont:[UIFont systemFontOfSize:18.f]];
		
		[self.view addSubview:_lbl];
		[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_lbl]|" options:NSLayoutFormatAlignmentMask metrics:nil views:NSDictionaryOfVariableBindings( _lbl )]];
		[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_lbl(44)]" options:NSLayoutFormatAlignmentMask metrics:nil views:NSDictionaryOfVariableBindings( _lbl )]];
	}
	
	if ( _driveBtn == nil )
	{
		UIImage *buttonImage = [[UIImage imageNamed:@"tanButton"] resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
		UIImage *buttonImageHighlight = [[UIImage imageNamed:@"tanButtonHighlight"] resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
		_driveBtn = [UIButton new];
		[_driveBtn setTranslatesAutoresizingMaskIntoConstraints:NO];
		[_driveBtn setTitle:@"Go To Driving Screen" forState:UIControlStateNormal];
		[_driveBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
		[[_driveBtn titleLabel] setFont:[UIFont systemFontOfSize:24.f]];
		
		// Set the background for any states you plan to use
		[_driveBtn setBackgroundImage:buttonImage forState:UIControlStateNormal];
		[_driveBtn setBackgroundImage:buttonImageHighlight forState:UIControlStateHighlighted];
		[_driveBtn addTarget:self action:@selector(driveBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
		
		[self.view addSubview:_driveBtn];	
		[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_driveBtn]-|" options:NSLayoutFormatAlignmentMask metrics:nil views:NSDictionaryOfVariableBindings( _driveBtn )]];
		[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_driveBtn(44)]-|" options:NSLayoutFormatAlignmentMask metrics:nil views:NSDictionaryOfVariableBindings( _driveBtn )]];
	}
	
	[self createSensrosContainer];
	
	if ( _apiManager == nil )
		_apiManager = [MBApiManager new];
}

#pragma mark
#pragma mark Custom
- (void)driveBtnTapped:(UIButton *)sender
{
	[ROOT_CONTROLLER switchToDrive];
}

- (void)createSensrosContainer
{
	if ( _lbl == nil || _driveBtn == nil || _sensorsContainer !=  nil || _robotInfoBtn != nil || _lightSensorBtn != nil || _touchSensorBtn != nil )
		return; // ASSERT( 0 );
	
	_sensorsContainer = [UIView new];
	[_sensorsContainer setTranslatesAutoresizingMaskIntoConstraints:NO];
	[_sensorsContainer setBackgroundColor:[UIColor clearColor]];
	
	[self.view addSubview:_sensorsContainer];
	
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_sensorsContainer]|" options:NSLayoutFormatDirectionMask metrics:nil views:NSDictionaryOfVariableBindings( _sensorsContainer )]];
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_lbl]-(0)-[_sensorsContainer]-(0)-[_driveBtn]" options:NSLayoutFormatDirectionMask metrics:nil views:NSDictionaryOfVariableBindings( _lbl, _sensorsContainer, _driveBtn )]];
	
	UIImage *btnImage = [[UIImage imageNamed:@"greenButton"] resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
	UIImage *btnImageHighlight = [[UIImage imageNamed:@"greenButtonHighlight"] resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
	
	// ROBOT INFO
	_robotInfoBtn = [UIButton new];
	[_robotInfoBtn setTranslatesAutoresizingMaskIntoConstraints:NO];
	[_robotInfoBtn setTitle:ROBOTINFO_BTN_TITLE forState:UIControlStateNormal];
	[_robotInfoBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[[_robotInfoBtn titleLabel] setFont:[UIFont systemFontOfSize:24.f]];
	
	[_robotInfoBtn setBackgroundImage:btnImage forState:UIControlStateNormal];
	[_robotInfoBtn setBackgroundImage:btnImageHighlight forState:UIControlStateHighlighted];
	[_robotInfoBtn addTarget:self action:@selector(sensorBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
	
	[_sensorsContainer addSubview:_robotInfoBtn];
	[_sensorsContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_robotInfoBtn]-|" options:NSLayoutFormatDirectionMask metrics:nil views:NSDictionaryOfVariableBindings( _robotInfoBtn )]];
	[_sensorsContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[_robotInfoBtn(44)]" options:NSLayoutFormatDirectionMask metrics:nil views:NSDictionaryOfVariableBindings( _robotInfoBtn )]];
	
	// LIGHT SENSOR
	_lightSensorBtn = [UIButton new];
	[_lightSensorBtn setTranslatesAutoresizingMaskIntoConstraints:NO];
	[_lightSensorBtn setTitle:LIGHTSENSORBTN_BTN_TITLE forState:UIControlStateNormal];
	[_lightSensorBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[[_lightSensorBtn titleLabel] setFont:[UIFont systemFontOfSize:24.f]];
	
	[_lightSensorBtn setBackgroundImage:btnImage forState:UIControlStateNormal];
	[_lightSensorBtn setBackgroundImage:btnImageHighlight forState:UIControlStateHighlighted];
	[_lightSensorBtn addTarget:self action:@selector(sensorBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
	
	[_sensorsContainer addSubview:_lightSensorBtn];
	[_sensorsContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_lightSensorBtn]-|" options:NSLayoutFormatDirectionMask metrics:nil views:NSDictionaryOfVariableBindings( _lightSensorBtn )]];
	[_sensorsContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_robotInfoBtn]-[_lightSensorBtn(44)]" options:NSLayoutFormatDirectionMask metrics:nil views:NSDictionaryOfVariableBindings( _robotInfoBtn, _lightSensorBtn )]];
	
	// TOUCH SENSOR
	_touchSensorBtn = [UIButton new];
	[_touchSensorBtn setTranslatesAutoresizingMaskIntoConstraints:NO];
	[_touchSensorBtn setTitle:TOUCHSENSORBTN_BTN_TITLE forState:UIControlStateNormal];
	[_touchSensorBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[[_touchSensorBtn titleLabel] setFont:[UIFont systemFontOfSize:24.f]];
	
	[_touchSensorBtn setBackgroundImage:btnImage forState:UIControlStateNormal];
	[_touchSensorBtn setBackgroundImage:btnImageHighlight forState:UIControlStateHighlighted];
	[_touchSensorBtn addTarget:self action:@selector(sensorBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
	
	[_sensorsContainer addSubview:_touchSensorBtn];
	[_sensorsContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[_touchSensorBtn]-|" options:NSLayoutFormatDirectionMask metrics:nil views:NSDictionaryOfVariableBindings( _touchSensorBtn )]];
	[_sensorsContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[_lightSensorBtn]-[_touchSensorBtn(44)]" options:NSLayoutFormatDirectionMask metrics:nil views:NSDictionaryOfVariableBindings( _lightSensorBtn, _touchSensorBtn )]];
}

- (void)sensorBtnTapped:(UIButton *)sender
{
	if ( _apiManager.strServiceURL == nil )
		[_apiManager setStrServiceURL:[ROOT_CONTROLLER strServiceURL]];
	
	if ( _apiManager.strSessionId == nil )
		[_apiManager setStrSessionId:[ROOT_CONTROLLER strConnectionId]];
	
	NSLog( @"[ROOT_CONTROLLER strServiceURL]: %@", [ROOT_CONTROLLER strServiceURL] );
	NSLog( @"[ROOT_CONTROLLER strConnectionId]: %@", [ROOT_CONTROLLER strConnectionId] );
	
	if ( [sender isEqual:_robotInfoBtn] )
	{
		[ROOT_CONTROLLER showWaitingAlertWithTitle:ROBOTINFO_WAITING_TITLE];
		[_apiManager apiRobotInfo:^(NSDictionary *dictRobotInfo)
		{
			[ROOT_CONTROLLER dismisWaitingAlert];
			
			NSDictionary *info = [dictRobotInfo objectForKey:@"robotInfo"];
			
			NSString *botStatus = [info objectForKey:@"status"];
			NSString *botName = [info objectForKey:@"bot_name"];
			NSString *botAddress = [info objectForKey:@"bot_address"];
			
			NSString *message = [NSString stringWithFormat:@"status: %@\n robot name: %@\n robotAddress: %@", botStatus, botName, botAddress];
			[self showAlertWithTitle:@"Robot Info" andMessge:message];
			
			NSLog( @"apiRobotInfoResult: %@", dictRobotInfo );
		}
		errorBlock:^(NSString *strError)
		{
			[ROOT_CONTROLLER dismisWaitingAlert];
			NSLog( @"apiRobotInfoError: %@", strError );
		}];
	}
	else if ( [sender isEqual:_lightSensorBtn] )
	{
		[ROOT_CONTROLLER showWaitingAlertWithTitle:LIGHTSENSORBTN_WAITING_TITLE];
		[_apiManager apiLightSensor:^(CGFloat lightIntensity)
		{
			[ROOT_CONTROLLER dismisWaitingAlert];
			[self showAlertWithTitle:@"Light Sensor" andMessge:[NSString stringWithFormat:@"Measured light intensity is: %.0f%%", lightIntensity]];
			NSLog( @"apiLightSensorResult: %.2f", lightIntensity );
		}
		errorBlock:^(NSString *strError)
		{
			[ROOT_CONTROLLER dismisWaitingAlert];
			NSLog( @"apiLightSensorError: %@", strError );
		}];
	}
	else if ( [sender isEqual:_touchSensorBtn] )
	{
		[ROOT_CONTROLLER showWaitingAlertWithTitle:TOUCHSENSORBTN_WAITING_TITLE];
		[_apiManager apiTouchSensorState:^(BOOL touched)
		{
			[ROOT_CONTROLLER dismisWaitingAlert];
			[self showAlertWithTitle:@"Touch sensor" andMessge:[NSString stringWithFormat:@"Sensor is %@", ( (touched) ? @"touched" : @"not touched" )]];
			NSLog( @"apiTouchSensorStateResult: %i", touched );
		}
		errorBlock:^(NSString *strError)
		{
			[ROOT_CONTROLLER dismisWaitingAlert];
			NSLog( @"apiTouchSensorStateError: %@", strError );
		}];
	}
}

- (void)showAlertWithTitle:(NSString *)title andMessge:(NSString *)message
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alert show];
	[alert release];
}

#pragma mark
#pragma mark Delegates
@end
