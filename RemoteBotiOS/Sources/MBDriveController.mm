//
//  MBDriveController.m
//  RemoteBot for iOS
//
//  Created by Michal Bínovský on 4/14/13.
//  Copyright (c) 2013 Michal Bínovský. All rights reserved.
//

#import "MBDriveController.h"
#import "MBViewController.h"
#import "ApiManager/MBApiManager.h"

static NSTimeInterval const MINIMUM_PROCESSING_COORDS_DELAY = 0.1;

#pragma mark
#pragma mark PrivateInterface
@interface MBDriveController()

	@property ( nonatomic, assign )	NSTimeInterval			lastProcessingTimestamp;
	@property ( nonatomic, retain )	MBApiManager			*apiManager;

	- (void)sensorBtnTapped:(UIButton *)sender;

@end

#pragma mark
#pragma mark MBDriveController
@implementation MBDriveController

@synthesize dpadController					= _dpadController;
@synthesize lastProcessingTimestamp			= _lastProcessingTimestamp;
@synthesize apiManager						= _apiManager;

- (id)init
{
	self = [super init];
	
	if ( self )
	{
		// Initialization code place here ...
	}
	
	return self;
}

- (void)dealloc
{
	[_dpadController release];
	[_apiManager release];
	[super dealloc];
}

#pragma mark
#pragma mark Override
// Place here UIKit overriden functions
- (void)viewDidLoad
{
	[super viewDidLoad];
	
	_lastProcessingTimestamp = 0;
	
	UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectZero];
	[lbl setTranslatesAutoresizingMaskIntoConstraints:NO];
	[lbl setBackgroundColor:[UIColor clearColor]];
	[lbl setText:@"RemoteBotIOS Client App\n Driving Screen"];
	[lbl setNumberOfLines:2];
	[lbl setAdjustsFontSizeToFitWidth:NO];
	[lbl setTextColor:[UIColor whiteColor]];
	[lbl setTextAlignment:NSTextAlignmentCenter];
	[lbl setFont:[UIFont systemFontOfSize:18.f]];
	
	[self.view addSubview:lbl];
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[lbl]|" options:NSLayoutFormatAlignmentMask metrics:nil views:NSDictionaryOfVariableBindings( lbl )]];
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[lbl(44)]" options:NSLayoutFormatAlignmentMask metrics:nil views:NSDictionaryOfVariableBindings( lbl )]];
	
	[lbl release];
	
	if ( _dpadController == nil )
	{
		_dpadController = [[MBDpadController alloc] initWithControllerImgName:@"dpad" andWithJoysticImgName:@"joystick"];
		[_dpadController setDelegate:self];
		[_dpadController setBackgroundColor:[UIColor clearColor]];
		
		[self.view addSubview:_dpadController];
		
		[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_dpadController]|" options:NSLayoutFormatDirectionMask metrics:nil views:NSDictionaryOfVariableBindings( _dpadController )]];
		[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_dpadController]|" options:NSLayoutFormatDirectionMask metrics:nil views:NSDictionaryOfVariableBindings( _dpadController )]];
	}
	
	UIImage *buttonImage = [[UIImage imageNamed:@"tanButton"] resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
	UIImage *buttonImageHighlight = [[UIImage imageNamed:@"tanButtonHighlight"] resizableImageWithCapInsets:UIEdgeInsetsMake(18, 18, 18, 18)];
	UIButton *sensorsBtn = [UIButton new];
	[sensorsBtn setTranslatesAutoresizingMaskIntoConstraints:NO];
	[sensorsBtn setTitle:@"Go To Sensors Screen" forState:UIControlStateNormal];
	[sensorsBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
	[[sensorsBtn titleLabel] setFont:[UIFont systemFontOfSize:24.f]];
	// Set the background for any states you plan to use
	[sensorsBtn setBackgroundImage:buttonImage forState:UIControlStateNormal];
	[sensorsBtn setBackgroundImage:buttonImageHighlight forState:UIControlStateHighlighted];
	[sensorsBtn addTarget:self action:@selector(sensorBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
	
	[self.view addSubview:sensorsBtn];
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-[sensorsBtn]-|" options:NSLayoutFormatAlignmentMask metrics:nil views:NSDictionaryOfVariableBindings( sensorsBtn )]];
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[sensorsBtn(44)]-|" options:NSLayoutFormatAlignmentMask metrics:nil views:NSDictionaryOfVariableBindings( sensorsBtn )]];
	
	[sensorsBtn release];
}

#pragma mark
#pragma mark Custom
- (void)sensorBtnTapped:(UIButton *)sender
{
	[ROOT_CONTROLLER switchToSensors];
}

#pragma mark
#pragma mark MBDpadControllerDelegate
- (void)dpadController:(MBDpadController *)controller touchedAtPoint:(CGPoint)point
{
	NSTimeInterval currentTimestamp = [NSDate timeIntervalSinceReferenceDate];
	if ( _lastProcessingTimestamp <= 0 || (currentTimestamp - _lastProcessingTimestamp) > MINIMUM_PROCESSING_COORDS_DELAY || CGPointEqualToPoint( point, CGPointZero ) )
	{
		// SEND COORDS TO SERVER
		_lastProcessingTimestamp = currentTimestamp;
//		NSLog( @"%@ %@ %@", NSStringFromClass( [self class] ), NSStringFromSelector( _cmd ), NSStringFromCGPoint( point ) );
		
		if ( _apiManager == nil )
		{
			_apiManager = [MBApiManager new];
			[_apiManager setStrServiceURL:[ROOT_CONTROLLER strServiceURL]];
			[_apiManager setStrSessionId:[ROOT_CONTROLLER strConnectionId]];
		}
		
		[_apiManager apiSendCommand:point completitionBlock:^(BOOL processed)
		{
			NSLog( @" PROCESSED: %@", (processed) ? @"TRUE" : @"FALSE" );
		}
		 errorBlock:^(NSString *strError)
		{
			NSLog( @"strError: %@", strError );
		}];
		
		return;
	}
	
//	NSLog( @"%@ %@ >>>> DROP <<<<", NSStringFromClass( [self class] ), NSStringFromSelector( _cmd ) );
}

@end
