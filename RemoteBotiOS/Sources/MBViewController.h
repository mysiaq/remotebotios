//
//  MBViewController.h
//  RemoteBot for iOS
//
//  Created by Michal Bínovský on 4/14/13.
//  Copyright (c) 2013 Michal Bínovský. All rights reserved.
//
@class MBSensorsController;
@class MBDriveController;

@interface MBViewController : UIViewController < UIAlertViewDelegate >
{
	#pragma mark
	#pragma mark @PublicIvars
	@public
		
	#pragma mark
	#pragma mark @ProtectedIvars
	@protected
		
	#pragma mark
	#pragma mark @PrivateIvars
	@private
}

	#pragma mark
	#pragma mark @Properties
	@property ( nonatomic, readonly )	NSString				*strServiceURL;
	@property ( nonatomic, readonly )	NSString				*strConnectionId;
	@property ( nonatomic, retain )		UIView					*contentView;
	@property ( nonatomic, retain )		MBSensorsController		*sensorsController;
	@property ( nonatomic, retain )		MBDriveController		*driveController;

	#pragma mark
	#pragma mark @MemberFunctions
	- (void)switchToDrive;
	- (void)switchToSensors;
	- (void)showWaitingAlertWithTitle:(NSString *)title;
	- (void)dismisWaitingAlert;

	#pragma mark
	#pragma mark @ClassFunctions
@end
