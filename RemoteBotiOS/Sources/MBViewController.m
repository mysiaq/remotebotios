//
//  MBViewController.m
//  RemoteBot for iOS
//
//  Created by Michal Bínovský on 4/14/13.
//  Copyright (c) 2013 Michal Bínovský. All rights reserved.
//

#import "MBViewController.h"
#import "MBSensorsController.h"
#import "MBDriveController.h"
#import "ApiManager/MBApiManager.h"

static int const NONINITIALIZED_APP_ALERT_TAG		= 99;
static int const SETTING_SERVICE_URL_ALERT_TAG		= 98;
static int const CONNECTION_REFUSED_ALERT_TAG		= 97;
static int const CONNECTION_ERROR_ALERT_TAG			= 96;
static int const WAINTING_ALERT_TAG					= 95;

#pragma mark
#pragma mark @PrivateInterface
@interface MBViewController ()

	@property ( nonatomic, retain )	NSString				*strServiceURL;
	@property ( nonatomic, retain )	NSString				*strConnectionId;
	@property ( nonatomic, retain ) UIAlertView				*waitingAlert;

	- (void)switchFromView:(UIView *)from toView:(UIView *)to;
	- (void)showSetupAlert;
	- (void)connectToRobot;

@end

#pragma mark
#pragma mark MBViewController
@implementation MBViewController

@synthesize strServiceURL			= _strServiceURL;
@synthesize strConnectionId			= _strConnectionId;
@synthesize contentView				= _contentView;
@synthesize sensorsController		= _sensorsController;
@synthesize driveController			= _driveController;
@synthesize waitingAlert			= _waitingAlert;

- (void)dealloc
{
	[_strServiceURL release];
	[_strConnectionId release];
	[_contentView release];
	[_sensorsController release];
	[_driveController release];
	[_waitingAlert release];
	[super dealloc];
}

#pragma mark
#pragma mark @Override
- (void)viewDidLoad
{
    [super viewDidLoad];
	
	if ( _driveController == nil )
	{
		_driveController = [[MBDriveController alloc] initWithNibName:@"MBDriveController" bundle:nil];
		[_driveController.view setTranslatesAutoresizingMaskIntoConstraints:NO];
	}
	
	if ( _sensorsController == nil )
	{
		_sensorsController = [[MBSensorsController alloc] initWithNibName:@"MBSensorsController" bundle:[NSBundle mainBundle]];
		[_sensorsController.view setTranslatesAutoresizingMaskIntoConstraints:NO];
	}
	
	if ( _contentView == nil && _driveController.view != nil )
	{
		_contentView = _driveController.view;
	
		[self.view addSubview:_contentView];
		
		[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_contentView]|" options:NSLayoutFormatDirectionMask metrics:nil views:NSDictionaryOfVariableBindings( _contentView )]];
		[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_contentView]|" options:NSLayoutFormatDirectionMask metrics:nil views:NSDictionaryOfVariableBindings( _contentView )]];
		
		[self showSetupAlert];
	}
	else
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Could not initialize application correctly!" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
		[alert setTag:NONINITIALIZED_APP_ALERT_TAG];
		[alert show];
		[alert release];
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)shouldAutorotate
{
	return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
	return UIInterfaceOrientationMaskAll;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	NSArray* arrParams = [NSArray arrayWithObjects:[NSNumber numberWithInt:toInterfaceOrientation], [NSNumber numberWithFloat:duration], nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFY_INTERFACEWILLROTATE object:arrParams];
}

#pragma mark
#pragma mark @Custom
- (void)switchToDrive
{
	[self switchFromView:_sensorsController.view toView:_driveController.view];
}

- (void)switchToSensors
{
	[self switchFromView:_driveController.view toView:_sensorsController.view];
}

- (void)switchFromView:(UIView *)from toView:(UIView *)to
{
	[UIView transitionFromView:from toView:to duration:0.45f options:UIViewAnimationOptionTransitionFlipFromBottom completion:^(BOOL finished)
	{
	}];
	
	[_contentView removeFromSuperview];
	_contentView = nil;
	_contentView = to;
	[[self view] addSubview:_contentView];
	
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_contentView]|" options:NSLayoutFormatDirectionMask metrics:nil views:NSDictionaryOfVariableBindings( _contentView )]];
	[self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_contentView]|" options:NSLayoutFormatDirectionMask metrics:nil views:NSDictionaryOfVariableBindings( _contentView )]];
}

- (void)showSetupAlert
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Setup" message:@"Please enter service URL" delegate:self cancelButtonTitle:@"Connect" otherButtonTitles:nil];
	[alert setAlertViewStyle:UIAlertViewStylePlainTextInput];
	[alert setTag:SETTING_SERVICE_URL_ALERT_TAG];
	
	UITextField *inputField = [alert textFieldAtIndex:0];
	[inputField setPlaceholder:@"http://255.255.255.255:8080"];
	[inputField setText:@"http://10.0.1.2:8080"];
	[alert show];
	[alert release];
}

#pragma mark
#pragma mark @UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
	switch ( alertView.tag )
	{
		case SETTING_SERVICE_URL_ALERT_TAG:
		{
			if ( _strServiceURL != nil )
			{
				[_strServiceURL release];
				_strServiceURL = nil;
			}
			
			self.strServiceURL = [[alertView textFieldAtIndex:0] text];
			
			if ( _strServiceURL.length <= 0 )
			{
				_strServiceURL = nil;
				
				[self showSetupAlert];
				return;
			}
			
			MBApiManager *apiManager = [MBApiManager new];
			[apiManager setStrServiceURL:_strServiceURL];
			[apiManager apiConnect:^( NSString *connectionId )
			 {
				 NSLog( @" connectionId: %@ ", connectionId );
				 
				 if ( [connectionId isEqualToString:@"Error"] )
				 {
					 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Connection to server refused because there is already running connection session, try again later." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
					 [alert setTag:CONNECTION_REFUSED_ALERT_TAG];
					 [alert show];
					 [alert release];
					 
					 return;
				 }
				 
				 if ( _strConnectionId != nil )
				 {
					 [_strConnectionId release];
					 _strConnectionId = nil;
				 }
				 
				 self.strConnectionId = connectionId;
				 
				 [self showWaitingAlertWithTitle:@"Connecting to robot"];
				 [self connectToRobot];
			 }
				errorBlock:^( NSString *strError )
			 {
				 NSLog( @"strError: %@", strError );
				 
				 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[@"Connection error: " stringByAppendingString:strError] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
				 [alert setTag:CONNECTION_ERROR_ALERT_TAG];
				 
				 [alert show];
				 [alert release];
			 }];
		}
			break;
			
		case CONNECTION_ERROR_ALERT_TAG:
		{
			[self showSetupAlert];
		}
			break;
			
		case CONNECTION_REFUSED_ALERT_TAG:
		{
			[self showSetupAlert];
		}
			break;
			
		case NONINITIALIZED_APP_ALERT_TAG:
		{
			exit( EXIT_FAILURE );
		}
			break;
		default:
			break;
	}
}

- (void)connectToRobot
{
	MBApiManager *apiManager = [MBApiManager new];
	[apiManager setStrServiceURL:_strServiceURL];
	[apiManager setStrSessionId:_strConnectionId];
	
	[apiManager apiBluetooth:^(BOOL connected)
	{
		[self dismisWaitingAlert];
		
		if ( !connected )
		{
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Connection to robot failed! Try again later." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
			[alert setTag:CONNECTION_ERROR_ALERT_TAG];
			[alert show];
			[alert release];
		}
		
	}
	errorBlock:^(NSString *strError)
	{
		NSLog( @"strError: %@", strError );
		[self dismisWaitingAlert];
		
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[@"Connection to robot failed: " stringByAppendingString:strError] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
		[alert setTag:CONNECTION_ERROR_ALERT_TAG];
		[alert show];
		[alert release];
	}];
	
}

- (void)showWaitingAlertWithTitle:(NSString *)title
{
	if ( _waitingAlert == nil )
		_waitingAlert = [[UIAlertView alloc] initWithTitle:title message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
	
	[self dismisWaitingAlert];
	
	[_waitingAlert setTag:WAINTING_ALERT_TAG];
	
	UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
	spinner.center = CGPointMake(139.5, 75.5); // .5 so it doesn't blur
	[_waitingAlert addSubview:spinner];
	[spinner startAnimating];
	
	[_waitingAlert show];
}

- (void)dismisWaitingAlert
{
	if ( _waitingAlert != nil )
		[_waitingAlert dismissWithClickedButtonIndex:0 animated:YES];
}

@end
