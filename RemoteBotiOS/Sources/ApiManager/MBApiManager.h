#import "../AsiHTTP/ASIHTTPRequest.h"

@interface MBApiManager : NSObject < ASIHTTPRequestDelegate >
{
	#pragma mark
	#pragma mark PublicIVars
	// HERE PLACE PUBLIC INSTANCE VARIABLES
		
	#pragma mark
	#pragma mark ProtectedIVars
	// HERE PLACE PROTECTED INSTANCE VARIABLES
		
	#pragma mark
	#pragma mark PrivateIVars
	@private
	SBJsonParser			*_parser;
    SBJsonWriter			*_writer;
}

	#pragma mark
	#pragma mark Properties
	@property ( nonatomic, retain ) NSString		*strServiceURL;
	@property ( nonatomic, retain )	NSString		*strSessionId;

	#pragma mark
	#pragma mark MemberFunctions
	- (void)apiConnect:( void (^) (NSString *connectionId) )completition errorBlock:( void (^) (NSString *strError))errorBlock;
	- (void)apiBluetooth:( void (^) (BOOL connected) )completition errorBlock:( void (^) (NSString *strError))errorBlock;
	- (void)apiRobotInfo:( void (^) (NSDictionary *dictRobotInfo) )completition errorBlock:( void (^) (NSString *strError))errorBlock;
	- (void)apiTouchSensorState:( void (^) (BOOL touched) )completition errorBlock:( void (^) (NSString *strError))errorBlock;
	- (void)apiLightSensor:( void (^) (CGFloat lightIntensity) )completition errorBlock:( void (^) (NSString *strError))errorBlock;
	- (void)apiSendCommand:(CGPoint)point completitionBlock:( void (^) (BOOL processed) )completition errorBlock:( void (^) (NSString *strError))errorBlock;

	#pragma mark
	#pragma mark ClassFunctions
	// HERE PLACE CLASS FUNCTIONS (Example: + (void)fooBaz;)
	
@end
