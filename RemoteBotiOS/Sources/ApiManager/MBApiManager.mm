#import "MBApiManager.h"

static NSString *const SERVICE_ERROR_SESSION_ID		= @"Error";
// SERVICE URI
static NSString *const SERVICE_URI					= @"/service/RestRemoteService/";
// APIs
static NSString *const API_CONNECT					= @"connect";
static NSString *const API_BLUETOOTH				= @"bluetooth";
static NSString *const API_ROBOT_INFO				= @"robotInfo";
static NSString *const API_TOUCH_SENSOR				= @"touchSensorState";
static NSString *const API_LIGHT_SENSOR				= @"lightSensor";
static NSString *const API_SEND_COMMAND				= @"sendCommand";
// REQUEST METHODS
static NSString *const REQUESTMETHOD_GET			= @"GET";
static NSString *const REQUESTMETHOD_POST			= @"POST";
static NSString *const REQUESTMETHOD_PUT			= @"PUT";
static NSString *const REQUESTMETHOD_DELETE			= @"DELETE";

#pragma mark
#pragma mark PrivateInterface
@interface MBApiManager()

	// API CONNECT
	@property ( copy )				void			(^apiConnectCompletitionBlock)(NSString *connectionId);
	@property ( copy )				void			(^apiConnectErrorBlock)(NSString *errorBlock);

	// API BLUETOOTH
	@property ( copy )				void			(^apiBluetoothCompletitionBlock)(BOOL connected);
	@property ( copy )				void			(^apiBluetoothErrorBlock)(NSString *errorBlock);

	// API ROBOT INFO
	@property ( copy )				void			(^apiRobotInfoCompletitionBlock)(NSDictionary *dictRobotInfo);
	@property ( copy )				void			(^apiRobotInfoErrorBlock)(NSString *errorBlock);

	// API TOUCH SENSOR STATE
	@property ( copy )				void			(^apiTouchsensorStateCompletitionBlock)(BOOL touched);
	@property ( copy )				void			(^apiTouchsensorErrorBlock)(NSString *errorBlock);

	// API LIGH SENSOR
	@property ( copy )				void			(^apiLightSensorCompletitionBlock)(CGFloat lightIntensity);
	@property ( copy )				void			(^apiLightSensorErrorBlock)(NSString *errorBlock);

	// API SEND COMMAND
	@property ( copy )				void			(^apiSendCommandCompletitionBlock)(BOOL processed);
	@property ( copy )				void			(^apiSendCommandErrorBlock)(NSString *errorBlock);

	- (void)initDefault;
	- (void)startRequestWithURL:(NSURL *)url methodType:(NSString *)method andData:(NSString *)jsonData;
	- (NSString *)servicePath;
	- (void)removeLastSlashFromServiceURL;
	- (NSString *)uuid;

@end

#pragma mark
#pragma mark MBApiManager
@implementation MBApiManager

@synthesize strServiceURL	=		_strServiceURL;
@synthesize strSessionId	=		_strSessionId;

- (id)init
{
	self = [super init];
	
	if ( self )
	{
		// Initialization code place here ...
		[self initDefault];
	}
	
	return self;
}

- (void)dealloc
{
	[_parser release];
    [_writer release];
	[_strServiceURL release];
	[_strSessionId release];
	[super dealloc];
}

#pragma mark
#pragma mark Override
// Place here NSObject overriden functions

#pragma mark
#pragma mark Custom
- (void)initDefault
{
	_strServiceURL = nil;
	
	if ( _parser == nil )
		_parser = [[SBJsonParser alloc] init];
	
	if ( _writer == nil )
		_writer = [[SBJsonWriter alloc] init];
	
	_writer.humanReadable = YES;
	_writer.sortKeys = YES;
}

- (NSString *)servicePath
{
	if ( _strServiceURL == nil || _strServiceURL.length <= 0 )
		return @"";
	
	return [_strServiceURL stringByAppendingString:SERVICE_URI];
}

- (void)removeLastSlashFromServiceURL
{
	if ( _strServiceURL == nil || _strServiceURL.length <= 0 )
		return;
	
	if ( [_strServiceURL characterAtIndex:[_strServiceURL length] - 1] == '/' )
	{
		NSString *strServiceURL = [_strServiceURL copy];
		[_strServiceURL release];
		_strServiceURL = nil;
		
		_strServiceURL = [strServiceURL substringToIndex:[strServiceURL length] - 1];
	}
}

- (NSString *)uuid
{
	NSString *uuid = nil;
	CFUUIDRef theUUID = CFUUIDCreate(kCFAllocatorDefault);
	if (theUUID)
	{
		uuid = NSMakeCollectable(CFUUIDCreateString(kCFAllocatorDefault, theUUID));
		[uuid autorelease];
		CFRelease(theUUID);
	}
	
	return uuid;
}

- (void)startRequestWithURL:(NSURL *)url methodType:(NSString *)method andData:(NSString *)jsonData
{
	ASIHTTPRequest *request = [ASIHTTPRequest requestWithURL:url];
	if ( _strSessionId == nil || _strSessionId.length <= 0 || [_strSessionId isEqualToString:SERVICE_ERROR_SESSION_ID] )
		[request addRequestHeader:@"cleint-id" value:[self uuid]];
	else
		[request addRequestHeader:@"session-id" value:_strSessionId];
	
	[request addRequestHeader:@"content-type" value:@"application/json"];
	[request setRequestMethod:method];
	
	if ( [method isEqualToString:REQUESTMETHOD_PUT] && jsonData != nil && jsonData.length > 0 )
	{
		[request appendPostData:[jsonData dataUsingEncoding:NSUTF8StringEncoding]];
	}
	
	[request setDelegate:self];
	[request startAsynchronous];
}

- (void)apiConnect:( void (^) (NSString *connectionId) )completition errorBlock:( void (^) (NSString *strError))errorBlock
{
	self.apiConnectCompletitionBlock = completition;
	self.apiConnectErrorBlock = errorBlock;
	
	NSString *jsonData = [_writer stringWithObject:[NSDictionary dictionaryWithObject:[self uuid] forKey:@"cleint-id"]];
	[self startRequestWithURL:[NSURL URLWithString:[[self servicePath] stringByAppendingString:API_CONNECT]] methodType:REQUESTMETHOD_PUT andData:jsonData];
}

- (void)apiBluetooth:( void (^) (BOOL connected) )completition errorBlock:( void (^) (NSString *strError))errorBlock
{
	self.apiBluetoothCompletitionBlock = completition;
	self.apiBluetoothErrorBlock = errorBlock;
	
	NSString *jsonData = [_writer stringWithObject:[NSDictionary dictionaryWithObject:[NSNumber numberWithBool:YES] forKey:@"run"]];
	[self startRequestWithURL:[NSURL URLWithString:[[self servicePath] stringByAppendingString:API_BLUETOOTH]] methodType:REQUESTMETHOD_PUT andData:jsonData];
}

- (void)apiRobotInfo:( void (^) (NSDictionary *dictRobotInfo) )completition errorBlock:( void (^) (NSString *strError))errorBlock
{
	self.apiRobotInfoCompletitionBlock = completition;
	self.apiRobotInfoErrorBlock = errorBlock;
	
	[self startRequestWithURL:[NSURL URLWithString:[[self servicePath] stringByAppendingString:API_ROBOT_INFO]] methodType:REQUESTMETHOD_GET andData:nil];
}

- (void)apiTouchSensorState:( void (^) (BOOL touched) )completition errorBlock:( void (^) (NSString *strError))errorBlock
{
	self.apiTouchsensorStateCompletitionBlock = completition;
	self.apiTouchsensorErrorBlock = errorBlock;
	
	[self startRequestWithURL:[NSURL URLWithString:[[self servicePath] stringByAppendingString:API_TOUCH_SENSOR]] methodType:REQUESTMETHOD_GET andData:nil];
}

- (void)apiLightSensor:( void (^) (CGFloat lightIntensity) )completition errorBlock:( void (^) (NSString *strError))errorBlock
{
	self.apiLightSensorCompletitionBlock = completition;
	self.apiLightSensorErrorBlock = errorBlock;
	
	[self startRequestWithURL:[NSURL URLWithString:[[self servicePath] stringByAppendingString:API_LIGHT_SENSOR]] methodType:REQUESTMETHOD_GET andData:nil];
}

- (void)apiSendCommand:(CGPoint)point completitionBlock:( void (^) (BOOL processed) )completition errorBlock:( void (^) (NSString *strError))errorBlock
{
	self.apiSendCommandCompletitionBlock = completition;
	self.apiSendCommandErrorBlock = errorBlock;

	NSString *jsonData = [_writer stringWithObject:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[NSNumber numberWithFloat:point.x], [NSNumber numberWithFloat:point.y], nil] forKeys:[NSArray arrayWithObjects:@"x", @"y", nil]]];
	[self startRequestWithURL:[NSURL URLWithString:[[self servicePath] stringByAppendingString:API_SEND_COMMAND]] methodType:REQUESTMETHOD_PUT andData:jsonData];
}


#pragma mark
#pragma mark ASIHTTPRequestDelegate
- (void)requestFinished:(ASIHTTPRequest *)request
{
	// Use when fetching text data
	NSLog( @"API '%@' processing finished", [[request url] lastPathComponent] );
	
	NSString *strApi = [[request url] lastPathComponent];
	NSString *strResponse = [request responseString];
	
	if ( [strApi isEqualToString:API_CONNECT] )
	{
		NSDictionary *rtnData = (NSDictionary *)[_parser objectWithString:strResponse];
		NSString *strSessionId = [rtnData objectForKey:@"sessionId"];
		
		if ( strSessionId == nil || strSessionId.length <= 0 )
		{
			_strSessionId = SERVICE_ERROR_SESSION_ID;
			self.apiConnectErrorBlock( strSessionId );
		}
		else
		{
			_strSessionId = strSessionId;
			self.apiConnectCompletitionBlock( strSessionId );
		}
	}
	else if ( [strApi isEqualToString:API_BLUETOOTH] )
	{
		NSDictionary *rtnData = (NSDictionary *)[_parser objectWithString:strResponse];
		
		if ( [rtnData objectForKey:@"running"] != nil )
		{
			BOOL bRunning = [[rtnData objectForKey:@"running"] boolValue];
			self.apiBluetoothCompletitionBlock( bRunning );
		}
		else
		{
			self.apiBluetoothErrorBlock( @"Error, connection to robot probably failed" );
		}
	}
	else if ( [strApi isEqualToString:API_ROBOT_INFO] )
	{
		NSDictionary *rtnData = (NSDictionary *)[_parser objectWithString:strResponse];
		
		if ( rtnData != nil )
			self.apiRobotInfoCompletitionBlock( rtnData );
		else
			self.apiRobotInfoErrorBlock( @"Error, no data returned from server" );
	}
	else if ( [strApi isEqualToString:API_TOUCH_SENSOR] )
	{
		NSDictionary *rtnData = (NSDictionary *)[_parser objectWithString:strResponse];
		
		if ( [rtnData objectForKey:@"state"] != nil )
		{
			BOOL bTouched = [[rtnData objectForKey:@"state"] boolValue];
			self.apiTouchsensorStateCompletitionBlock( bTouched );
		}
		else
		{
			self.apiTouchsensorErrorBlock( @"Error, no data returned from server" );
		}
	}
	else if ( [strApi isEqualToString:API_LIGHT_SENSOR] )
	{
		NSDictionary *rtnData = (NSDictionary *)[_parser objectWithString:strResponse];
		
		if ( [rtnData objectForKey:@"value"] !=  nil )
		{
			CGFloat fLightIntensity = [[rtnData objectForKey:@"value"] floatValue];
			self.apiLightSensorCompletitionBlock( fLightIntensity );
		}
		else
		{
			self.apiLightSensorErrorBlock( @"Error, no data returned from server" );
		}
	}
	else if ( [strApi isEqualToString:API_SEND_COMMAND] )
	{
		NSDictionary *rtnData = (NSDictionary *)[_parser objectWithString:strResponse];
		
		if ( [rtnData objectForKey:@"processed"] != nil )
		{
			BOOL bProcessed = [[rtnData objectForKey:@"processed"] boolValue];
			self.apiSendCommandCompletitionBlock( bProcessed );
		}
		else
		{
			self.apiSendCommandErrorBlock( @"Error, command probably not processed" );
		}
	}
	else
	{
		// _ASSERT( 0 )
	}
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
	NSString *strApi = [[request url] lastPathComponent];
	NSString *error = [[request error] domain];
	
	
	if ( [strApi isEqualToString:API_CONNECT] )
	{
		self.apiConnectErrorBlock( error );
	}
	else if ( [strApi isEqualToString:API_BLUETOOTH] )
	{
		self.apiBluetoothErrorBlock( error );
	}
	else if ( [strApi isEqualToString:API_ROBOT_INFO] )
	{
		self.apiRobotInfoErrorBlock( error );
	}
	else if ( [strApi isEqualToString:API_TOUCH_SENSOR] )
	{
		self.apiTouchsensorErrorBlock( error );
	}
	else if ( [strApi isEqualToString:API_LIGHT_SENSOR] )
	{
		self.apiLightSensorErrorBlock( error );
	}
	else if ( [strApi isEqualToString:API_SEND_COMMAND] )
	{
		self.apiSendCommandErrorBlock( error );
	}
	else
	{
		// ASSERT( 0 )
	}
}

@end