//
//  MBSensorsController.h
//  RemoteBot for iOS
//
//  Created by Michal Bínovský on 4/22/13.
//  Copyright (c) 2013 Michal Bínovský. All rights reserved.
//
@class MBViewController;
@class MBApiManager;

@interface MBSensorsController : UIViewController
{
	#pragma mark
	#pragma mark PublicIVars
	@public
		
	#pragma mark
	#pragma mark ProtectedIVars
	@protected
		
	#pragma mark
	#pragma mark PrivateIVars
	@private
}

	#pragma mark
	#pragma mark Properties
	@property ( nonatomic, readonly )	MBApiManager			*apiManager;
	
	#pragma mark
	#pragma mark MemberFunctions
	
	#pragma mark
	#pragma mark ClassFunctions
	
@end
