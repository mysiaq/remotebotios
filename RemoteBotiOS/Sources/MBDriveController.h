//
//  MBDriveController.h
//  RemoteBot for iOS
//
//  Created by Michal Bínovský on 4/14/13.
//  Copyright (c) 2013 Michal Bínovský. All rights reserved.
//

#import "DPad/MBDpadController.h"

@class MBDpadController;
@class MBApiManager;

@interface MBDriveController : UIViewController < MBDpadControllerDelegate >
{
	#pragma mark
	#pragma mark PublicIVars
	@public
		
	#pragma mark
	#pragma mark ProtectedIVars
	@protected
		
	#pragma mark
	#pragma mark PrivateIVars
	@private
}

	#pragma mark
	#pragma mark Properties
	@property ( nonatomic, retain )		MBDpadController		*dpadController;
	@property ( nonatomic, readonly )	MBApiManager			*apiManager;
	
	#pragma mark
	#pragma mark MemberFunctions
	
	#pragma mark
	#pragma mark ClassFunctions
	
@end
