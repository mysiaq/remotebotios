//
// Prefix header for all source files of the 'RemoteBot for iOS' target in the 'RemoteBot for iOS' project
//
#import <Availability.h>

#ifndef __IPHONE_4_0
#warning "This project uses features only available in iOS SDK 4.0 and later."
#endif

#ifdef __OBJC__

	#import <UIKit/UIKit.h>
	#import <Foundation/Foundation.h>
	#import <QuartzCore/QuartzCore.h>
	#import "SBJson.h"
	#import "MBAppDelegate.h"
	#import "Macros.h"
	#import "Common.h"

#endif


